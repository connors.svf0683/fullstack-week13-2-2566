import { createRouter,createWebHistory } from "vue-router";

const routes = [
    {
        path:'/',
        name:'home',
        component:() => import('../views/HomeView.vue'),
    },
    {
        path:'/about',
        name:'about',
        component:() => import('../views/AboutView.vue'),
    },
    {
        path:'/option-api',
        name:'OptionAPI',
        component:() => import('../components/OptionView.vue'),
    },
    {
        path:'/composition-api',
        name:'CompositionAPI',
        component:() => import('../components/CompositionView.vue'),
    },
    {
        path:'/reactive-view',
        name:'reactive-view',
        component:() => import('@/components/ReactiveView.vue'),
    },
    {
        path:'/:pathMatch(.*)*',
        name:'NotFound',
        component:() => import('../views/NotFound.vue'),
    },
    {
        path:'/twoway',
        name:'twoway',
        component:() => import('@/components/twoWay.vue')
    },
];

const router = createRouter({
    history:createWebHistory(),
    routes,
})

export default router
